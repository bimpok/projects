# Boggle Game

This is a simple boggle game. A 4*4 board is drawn with random letters. User input the matching word from the board to the input box. Every input is validated -- valid entries are populated to the found word grid else validation error is shown.

Front end for the game is developed using react native and back end service is developed in Ruby on Rails.

## Getting Started

Full project is available in following Bitbucket public repository.

SSH: git clone git@bitbucket.org:bimpok/projects.git
HTTPS: git clone https://bimpok@bitbucket.org/bimpok/projects.git

Under assurace, find the two directories.

* boggle: this is the react app
* boggle-service: this is rails service acting as a backend for the game

### Prerequisites

We have used react native and Ruby on Rails with Rails 6.0.2.2 and ruby 2.6.6p146. NPM version is 6.13.4. 

```
A list of gems used in RoR service are listed under Gemfile located in the project root.
```

### Installing

```
Install npm, rails, postgresql
```

Inside boggle-service directory, run command bundle install

```
Start rails server using command rails s
```

Start react web pack using command npm start

## Running the tests

RoR tests are written in RSpec test suite. Tests can be run using the command bundle exec rspec.

## Playing the game

React app is hosted in port 3006. 

```
Open http://localhost.3006
```
```
Click start play button to load the board
```
```
Find the word within 60 seconds. After 60 seconds, word can't be entered.
```


## To Dos

* Authentication
* Better tests and better exception handling
* Service queue to handle multiple concurrent requests
