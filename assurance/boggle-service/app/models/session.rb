class Session < ApplicationRecord
  belongs_to :boggle
  validates_presence_of :entry
  validates_presence_of :score
end
