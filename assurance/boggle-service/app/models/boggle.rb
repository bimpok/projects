class Boggle < ApplicationRecord
  has_many :sessions, dependent: :destroy
  validates_presence_of :letter_grid
  validates_presence_of :valid_entries
  validates_presence_of :access_token
  validates_presence_of :expiry_dt
end
