class SessionsController < ApplicationController

  before_action :set_boggle
  before_action :set_boggle_session, only: [:show, :update, :destroy]

  #GET /boggles/:boggle_id/Sessions

  def index
    json_response(@boggle.sessions)
  end

  #GET /boggles/:boggle_id/sessions/:id
  def show
    json_response(@session)
  end

  #POST /boggles/:boggle_id/Sessions
  def create

    # if @boggle.valid_entries.include? session_params[:entry]
    #   @is_valid = true
    # else
    #   @is_valid = false

    @is_valid = @boggle.valid_entries.include? session_params[:entry].upcase

    if(@is_valid)
      @entry = @boggle.sessions.create!(session_params)
    end

    json_response({"valid": @is_valid}, :created)
  end

  # PUT /boggles/:boggle_id/sessions/:id
  def update
    @session.update(session_params)
    head :no_content
  end

  private

  def session_params
    params.permit(:entry, :score)
  end

  def set_boggle
    @boggle = Boggle.find(params[:boggle_id])
  end

  def set_boggle_session
    @session = @boggle.sessions.find_by!(id: params[:id]) if @boggle
  end

end
