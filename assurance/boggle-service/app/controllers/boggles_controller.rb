require 'rest-client'

class BogglesController < ApplicationController

  before_action :set_boggle, only: [:show, :update, :destroy]
  # before_save :create_boggle

  #GET /boggles
  def index
    @boggles = Boggle.all
    json_response(@boggles)
  end

  #POST /boggles
  def create
    o = [('A'..'Z')].map(&:to_a).flatten
    letter_grid = (0...16).map { o[rand(o.length)] }.join(',')
    letters_nocomma = letter_grid.gsub(/[\s,]/ ,"")
    valid_entries = get_boggle(letters_nocomma)

    @boggle = Boggle.create!(
      user_id: 'testuser',
      letter_grid: letter_grid,
      valid_entries: valid_entries,
      expiry_dt: '2020-04-26 02:12:00',
      access_token: 'fj43jfakfjhsdf34rn' ## TODO: Generate new access_token for each request
    )
    json_response({"boggle_id": @boggle.id, "access_token": @boggle.access_token}, :created)
  end

  #GET /boggles/:id
  def show
    json_response(@boggle)
  end

  #PUT /boggles/:id
  def update
    @boggle.update(boggle_update_params)
    head :no_content
  end

  #DELETE /boggles/:id
  def destroy
    @boggle.destroy
    head :no_content
  end

  private

  def boggle_params
    #whitelist params
    params.permit(:letter_grid, :access_token, :expiry_dt, :valid_entries)
  end

  def boggle_update_params
    #whitelist params
    params.permit(:total_score)
  end

  def set_boggle
    @boggle = Boggle.find(params[:id])
  end

  def get_boggle(letter_grid)
    url = "https://codebox-boggle-v1.p.rapidapi.com/" + letter_grid
    body  = RestClient.get(url, headers={
      "x-rapidapi-host": "codebox-boggle-v1.p.rapidapi.com",
	    "x-rapidapi-key": "4387d99148mshcebbf71df2653aep171670jsn4032390ffffb"
      })
    valid_entries = JSON.parse(body)
    return valid_entries
  end

end
