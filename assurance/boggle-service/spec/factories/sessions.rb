FactoryBot.define do
  factory :session do
    entry {Faker::Lorem.word}
    score {10}
    boggle_id nil
  end
end
