FactoryBot.define do
  factory :boggle do
    letter_grid { Faker::Alphanumeric.alpha(number: 16) }
    valid_entries { '["SIS", "SIR", "DISS", "HISSY", "IFS", "SHRIS", "FISH", "SKY", "IDS", "SRIS", "DIRK", "SISSY", "DISH", "SHIRK", "DIS", "DISHY", "HIS", "RIFS", "KRIS", "SHIRKS", "HID", "HISS", "RIF", "RID", "RIDS", "FIR", "DIRKS", "SHY", "SRI", "SHRI", "SIRS", "IRK", "FID", "SHH", "FRY", "FIRS", "FIDS", "IRKS", "FISHY"]' }
    # user_id { 'ds4fjf424kdskghdfdsj' } #Generate browser fingerprinting
    access_token { 'ds4fjf424kdskghdfdsj' }
    expiry_dt { Time.now.utc.iso8601 }
  end
end
