require 'rails_helper'

RSpec.describe 'Boggle API', type: :request do

  let!(:boggles) { create_list(:boggle, 10) }
  let(:boggle_id) { boggles.first.id }

  # GET /boggles
  describe 'GET /boggles' do
    #make http GET request before each example
    before { get '/boggles' }

    it 'returns boggles' do
      expect(json).not_to be_empty
      expect(json.size).to eq(10)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end

  end

  describe 'GET /boggles/:id' do
    before { get "/boggles/#{boggle_id}" }

    context 'when the record exists' do
      it 'returns the boggle' do
        expect(json).not_to be_empty
        expect(json['id']).to eq(boggle_id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let (:boggle_id) { 100 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match (/Couldn't find Boggle/)
      end
    end

  end

  #test suite for POST /boggles

  describe 'POST /boggles' do
    let(:valid_attributes) {
      {
        letter_grid: 'SHDYKFHSYRISSSHD',
        access_token: 'ds4fjf424kdskghdfdsj',
        expiry_dt: Time.now.utc.iso8601,
        valid_entries: '["SIS", "SIR", "DISS", "HISSY", "IFS", "SHRIS", "FISH", "SKY", "IDS", "SRIS", "DIRK", "SISSY", "DISH", "SHIRK", "DIS", "DISHY", "HIS", "RIFS", "KRIS", "SHIRKS", "HID", "HISS", "RIF", "RID", "RIDS", "FIR", "DIRKS", "SHY", "SRI", "SHRI", "SIRS", "IRK", "FID", "SHH", "FRY", "FIRS", "FIDS", "IRKS", "FISHY"]'
      }
    }

    context 'when the request is valid' do
      before { post '/boggles', params: valid_attributes }

      it 'creates a boggle' do
        expect(json['letter_grid']).to eq('SHDYKFHSYRISSSHD')
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is invalid' do
      before { post '/boggles', params: { letter_grid: 'Foobar' } }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(response.body)
          .to match(/Validation failed: Valid entries can't be blank/)
      end

    end
  end

  # Test suite for PUT /boggles/:id
  describe 'PUT /boggles/:id' do
    let(:valid_attributes) { { total_score: 50 } }

    context 'when the record exists' do
      before { put "/boggles/#{boggle_id}", params: valid_attributes }

      it 'updates the record' do
        expect(response.body).to be_empty
      end

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
    end
  end

end
