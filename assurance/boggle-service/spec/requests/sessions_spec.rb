# spec/requests/sessions_spec.rb
require 'rails_helper'

RSpec.describe 'Sessions API' do
  # Initialize the test data
  let!(:boggle) { create(:boggle) }
  let!(:sessions) { create_list(:session, 20, boggle_id: boggle.id) }
  let(:boggle_id) { boggle.id }
  let(:id) { sessions.first.id }

  # Test suite for GET /boggles/:boggle_id/sessions
  describe 'GET /boggles/:boggle_id/sessions' do
    before { get "/boggles/#{boggle_id}/sessions" }

    context 'when boggle exists' do
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'returns all boggle sessions' do
        expect(json.size).to eq(20)
      end
    end

    context 'when boggle does not exist' do
      let(:boggle_id) { 0 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Boggle/)
      end
    end
  end

  # Test suite for GET /boggles/:boggle_id/sessions/:id
  describe 'GET /boggles/:boggle_id/sessions/:id' do
    before { get "/boggles/#{boggle_id}/sessions/#{id}" }

    context 'when boggle session exists' do
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'returns the session' do
        expect(json['id']).to eq(id)
      end
    end

    context 'when boggle session does not exist' do
      let(:id) { 0 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Session/)
      end
    end

  end

  # Test suite for POST /boggles/:boggle_id/sessions
  describe 'POST /boggles/:boggle_id/sessions' do
    let(:valid_attributes) { { entry: 'Visit', score: 5 } }

    context 'when request attributes are valid' do
      before { post "/boggles/#{boggle_id}/sessions", params: valid_attributes }

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when an invalid request' do
      before { post "/boggles/#{boggle_id}/sessions", params: {} }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a failure message' do
        expect(response.body).to match(/Validation failed: Entry can't be blank/)
      end
    end
  end

  # Test suite for PUT /boggles/:boggle_id/sessions/:id
  describe 'PUT /boggles/:boggle_id/sessions/:id' do
    let(:valid_attributes) { { entry: 'Hulala' } }

    before { put "/boggles/#{boggle_id}/sessions/#{id}", params: valid_attributes }

    context 'when session exists' do
      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end

      it 'updates the session' do
        updated_session = Session.find(id)
        expect(updated_session.entry).to match(/Hulala/)
      end
    end

    context 'when the session does not exist' do
      let(:id) { 0 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Session/)
      end
    end
  end


end
