require 'rails_helper'

RSpec.describe Boggle, type: :model do
  # it { should have_many(:sessions).dependent(:destroy) }
  it { should validate_presence_of(:letter_grid) }
  it { should validate_presence_of(:valid_entries) }
end
