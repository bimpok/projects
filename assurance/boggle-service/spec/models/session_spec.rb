require 'rails_helper'

RSpec.describe Session, type: :model do
    it { should belong_to(:boggle) }
    it { should validate_presence_of(:entry) }
    it { should validate_presence_of(:score) }
end
