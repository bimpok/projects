# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_04_26_153145) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "boggles", force: :cascade do |t|
    t.string "letter_grid"
    t.string "created_by"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.json "valid_entries"
    t.string "user_id"
    t.datetime "expiry_dt"
    t.integer "total_score"
    t.string "access_token"
  end

  create_table "entries", force: :cascade do |t|
    t.string "word"
    t.integer "length"
    t.integer "session_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["session_id"], name: "index_entries_on_session_id"
  end

  create_table "sessions", force: :cascade do |t|
    t.string "user_id"
    t.string "access_token"
    t.datetime "expiry"
    t.integer "score"
    t.integer "boggle_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "entry"
    t.index ["boggle_id"], name: "index_sessions_on_boggle_id"
  end

  add_foreign_key "entries", "sessions"
  add_foreign_key "sessions", "boggles"
end
