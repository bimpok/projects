class AddValidEntriesToBoggles < ActiveRecord::Migration[6.0]
  def change
    add_column :boggles, :valid_entries, :json
  end
end
