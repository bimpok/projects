class CreateSessions < ActiveRecord::Migration[6.0]
  def change
    create_table :sessions do |t|
      t.string :user_id
      t.string :access_token
      t.datetime :expiry
      t.integer :score
      t.references :boggle, null: false, foreign_key: true

      t.timestamps
    end
  end
end
