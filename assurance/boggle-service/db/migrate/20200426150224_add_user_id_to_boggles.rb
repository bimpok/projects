class AddUserIdToBoggles < ActiveRecord::Migration[6.0]
  def change
    add_column :boggles, :user_id, :string
  end
end
