class AddExpiryDtToBoggles < ActiveRecord::Migration[6.0]
  def change
    add_column :boggles, :expiry_dt, :datetime
    add_column :boggles, :add_total_score_to_boggles, :string
    add_column :boggles, :total_score, :integer
  end
end
