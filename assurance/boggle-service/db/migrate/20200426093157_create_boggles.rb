class CreateBoggles < ActiveRecord::Migration[6.0]
  def change
    create_table :boggles do |t|
      t.string :letter_grid
      t.string :created_by

      t.timestamps
    end
  end
end
