class AddAccessTokenToBoggles < ActiveRecord::Migration[6.0]
  def change
    add_column :boggles, :access_token, :string
  end
end
