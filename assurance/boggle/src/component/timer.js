import React, { Component } from "react";
import "../styles.css";

class Timer extends Component {
	
  constructor(props) {
    super(props);
  }

  render() {
    
    return (
      <div>
        <strong>TIMER: {this.props.counter}</strong>
      </div>
    );
  }
}

export default Timer;
