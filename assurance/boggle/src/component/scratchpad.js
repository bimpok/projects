import React, {Component} from "react";
import "../styles.css";
import Board from "./board";
import Input from "./input";
import Timer from "./timer";
import ScoreCard from "./scorecard";

class ScratchPad extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      score: 0,
      counter: 60,
      allWords: [],
      letterGrid: [],
      boggleId: 0,
      isEntryValid: true
    }
    this.countDown = this.countDown.bind(this);
    this.validateAndAdd = this.validateAndAdd.bind(this);
    this.chunkArray = this.chunkArray.bind(this);
    this.rotateBoard = this.rotateBoard.bind(this);
  }

  countDown() {
    this.state.counter > 0 && this.setState({
      counter: this.state.counter - 1
    });
  }

  chunkArray(array, size) {
    const chunked_arr = [];
    for (let i = 0; i < array.length; i++) {
      const last = chunked_arr[chunked_arr.length - 1];
      if (!last || last.length === size) {
        chunked_arr.push([array[i]]);
      } else {
        last.push(array[i]);
      }
    }
    return chunked_arr;
  }

  rotateBoard(e) {
    e.preventDefault()
    let letterGridTemp = []
    let firstGrid = []
    console.log('rotate...')
    this.state.letterGrid.map((value,key) => {
      console.log(value, key, this.state.letterGrid.length)
      // this.setState({letterGrid: this.state.letterGrid[key]})
      if (key > 0) {
        letterGridTemp.push(value)
      }
      else {
        firstGrid = value
      }
    })
    letterGridTemp.push(firstGrid)
    console.log(letterGridTemp)
    console.log(this.state.letterGrid)
    this.setState({ letterGrid: letterGridTemp })
  }

  async componentDidMount() {

    const requestOptions = {
        method: 'POST',
        headers: {},
        body: JSON.stringify({ user_id: 'testuser' })
    }

    const response = await fetch('http://localhost:3000/boggles', requestOptions)
    const boggle = await response.json()
    // console.log(boggle.boggle_id)
    await this.setState({boggleId: boggle.boggle_id})

    const url = 'http://localhost:3000/boggles/'.concat(this.state.boggleId)
    fetch(url)
        .then((res) => {
          return res.json();
        })
        .then((data) => {
          var letterGridObj = data.letter_grid.split(",")
          // console.log(typeof(letterGridObj))
          var chunked_letter_arr = this.chunkArray(letterGridObj,4)

          // console.log(chunked_letter_arr)
          this.setState({ letterGrid: chunked_letter_arr })
        })
        .catch(console.log);
    // console.log(this.state.letterGrid)

    this.state.counter > 0 && setInterval(() => this.countDown(),1000);

  }


  validateAndAdd(e) {

    if(e.keyCode == 13) {
      console.log(e.target.value);
      e.preventDefault();

      const requestOptions = {
          method: 'POST',
          headers: {'Content-Type': 'application/json; charset=utf-8', 'Access-Control-Allow-Origin': 'origin'},
          body: JSON.stringify({ entry: e.target.value, score: e.target.value.length })
      }

      console.log(requestOptions)

      const url = 'http://localhost:3000/boggles/'.concat(this.state.boggleId).concat('/sessions')

      fetch(url, requestOptions)
        .then((res) => {
          if (!res.ok) throw new Error(res.status)
          else return res.json();
        })
        .then((data) => {
          console.log(data.valid)
          this.setState({ isEntryValid: data.valid })
        })
        .catch((error) => {
          console.log('Error: ' + error)
        })

      // e.preventDefault();
      //
      // let allWords = [...this.state.allWords];
      // allWords.push({
      //   currentWord: e.target.value,
      //   wordLength: e.target.value.length
      // });
      //
      // this.setState({
      //   allWords: allWords,
      //   score: this.state.score + e.target.value.length
      // });
      //
      e.target.value = '';
    }
  }

  render() {
    // console.log(this.state.boggleId)
      return (
      <div>
        <div className="row column">
          <Board letters = {this.state.letterGrid}/>
          <Input counter = {this.state.counter} validateAndAdd = {this.validateAndAdd} isEntryValid = {this.state.isEntryValid} rotateBoard = {this.rotateBoard}/>
        </div>
        <div className="row column">
          <Timer counter = {this.state.counter}/>
          {this.state.boggleId > 0 && <ScoreCard boggle_id = {this.state.boggleId} allWords = {this.state.allWords} score = {this.state.score} counter = {this.state.counter}/>}
        </div>
      </div>
    );
  }
}

export default ScratchPad;
