import React from "react";
import "../styles.css";

class ScoreCard extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      allWords: [],
      boggleId: this.props.boggle_id
    }

    // this.setState({boggleId: this.props.bundle_id})
    console.log(this.state.boggleId)
    console.log(this.state.counter)
  }


  componentDidUpdate() {

    const url = 'http://localhost:3000/boggles/'.concat(this.state.boggleId).concat('/sessions')
    console.log(url)

    this.props.counter > 0 &&
    fetch(url)
        .then((res) => {
          return res.json();
        })
        .then((data) => {

          this.setState({
            allWords: [],
            score: 0
          });

          let allWords = [...this.state.allWords]
          let scoreTotal = 0
          data.map((entry,key) => {
            allWords.push({
              currentWord: entry.entry,
              wordLength: entry.score
            })

            scoreTotal = scoreTotal + entry.score

            this.setState({
              allWords: allWords,
              score: scoreTotal
            });
          })
        })
        .catch(console.log);
  }

  render() {
    // console.log(this.props.boggle_id)
    return (
      <div>
        <strong>SCORE: {this.state.score}</strong>
        <table align = "center" border = "1px">
        <tbody>
          <tr>
            <th>Word</th>
            <th>Score</th>
          </tr>
          {this.state.allWords.map((word,key) => {

            return (

                <tr key = {key}>
                  <td>{word.currentWord}</td>
                  <td>{word.wordLength}</td>
                </tr>
              );

          })}
        </tbody>
        </table>
      </div>
    );
  }

}

export default ScoreCard;
