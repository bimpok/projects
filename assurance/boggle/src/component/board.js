import React from "react";
import "../styles.css";

class Board extends React.Component {

	constructor(props) {
		super(props);
	}

	render() {
		const letterDiv = x => <div key = {x.id} className="letter-grid letter-cube">{x}</div>;
		const letterGrids = this.props.letters.map((letters,key) => (
		<div key = {key}>{letters.map(letterDiv)}</div>
  			));
  		return <div className="board">{letterGrids}</div>;
	}
}

export default Board;
