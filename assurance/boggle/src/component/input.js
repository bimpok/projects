import React from "react";
import "../styles.css";

class Input extends React.Component {

  constructor(props) {
    super(props);
  }

  render () {
    return (
      <div className="input-box">
        <form>
          <label>
            <strong>Type the word and hit enter</strong>
          </label>
          <input type="text" onKeyDown = {this.props.validateAndAdd} disabled = {this.props.counter == 0 ? 1 : 0}/>
          <div><span class="label danger">{!this.props.isEntryValid && "Invalid entry..."}</span></div>
          <button className="button rotateBtn" type="rotateButton" onClick = {this.props.rotateBoard}>Rotate</button>
        </form>
      </div>
    );
  }
}

export default Input;
