import React from "react"

const StartGameButton = props => {
  return (
    <div>
    <div><label>Boggle Game</label></div>
    <div><button onClick={props.startGame}>Start Game</button></div>
    </div>
  )
}

export default StartGameButton
