import React, { Component } from "react"
import "./styles.css"
import ScratchPad from "./component/scratchpad"
import StartGameButton from "./component/start"

class App extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      isBoardEmpty: true,
      gameId: 0
    }
  }

  triggerStartGameState = (e) => {
    e.preventDefault();
    // const requestOptions = {
    //     method: 'POST',
    //     headers: {},
    //     body: JSON.stringify({ user_id: 'testuser' })
    // }
    //
    // fetch('http://localhost:3000/boggles', requestOptions)
    //   .then(res => res.json())
    //   .then(data => this.setState({ gameId: data.game_id }))
    //   .then(console.log(this.state.gameId))
    //   .catch(console.log)

    this.setState({
      ...this.state,
      isBoardEmpty: false,
      isStartGameState: true
    })

    // console.log(this.state.gameId)
  }

  render() {
  	return (
    <div className="App">
      {this.state.isBoardEmpty && <StartGameButton startGame = {this.triggerStartGameState} />}
      {this.state.isStartGameState && <ScratchPad game_id = {this.state.gameId} />}
    </div>
  	)
  }

}

export default App
